%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ARQUITECTURAS AVANZADAS%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Arquitecturas avanzadas}
\section{Memoria caché}
\subsection{Problema}
\begin{itemize}
\item La CPU gasta mucho tiempo esperando el envío de datos desde y hacia la memoria.
\item Aún cuando la CPU sea rápida, el acceso a memoria puede hacer mucho mas lenta la ejecución de un programa.
\item  ?`C\'omo tener un acceso mas rápido a la memoria?
\begin{itemize}
\item Memoria dinámica (DRAM): barata, pero lenta.
\item Memoria estática (SRAM): rápida, pero cara.
\end{itemize}
\end{itemize}
\subsection{Experimento}
\begin{itemize}
\item Descomponer la memoria requerida por un programa en líneas de 4, 16, 32 o 64 bytes de direcciones contiguas.
\item Contabilizar el número de accesos a cada una de las líneas durante un intervalo de ejecución de un programa.
\item Hacer un ránking de las líneas mas accesadas
\item Hecho empírico 1: El 98\% de los accesos se concentran en el 3\% de las líneas.
\end{itemize}
\newpage


\subsection{Localidad espacial}
\begin{itemize}
\item Si se hace un acceso a un elemento de memoria, hay alta probabilidad de que se hará otro acceso a elementos que se encuentran cerca en el futuro próximo.
\item Usando este fenómeno podemos tener accesos más rápidos a la memoria y por lo tanto una cpu más rapida.
\item Idea 1
\begin{itemize}
\item memoria estática (el caché) de pequeño tamaño (Sc) para guardar las líneas más accesadas ahora.
\item El resto se almacena en la memoria dinamica (Tamaño Sm, con Sc $\ll$ Sm).
\end{itemize}
\end{itemize}
\subsubsection{Problema}
\begin{itemize}
\item ¿Cómo saber cuales serán las líneas mas accesadas durante el intervalo de ejecución?
\item Hecho empírico 2:
\begin{itemize}
\item un buen predictor de las líneas que serán más accesadas en el futuro cercano es el conjunto de las líneas que fueron accesadas en el pasado más reciente.
\end{itemize}
\end{itemize}
\subsection{Localidad temporal}
Si se hace un acceso a un elemento de memoria, hay alta probabilidad de que se haga otro acceso a este elemento en el futuro próximo.
\begin{center}
\includegraphics[scale=0.55]{f294}
\end{center}
\newpage


Idea 2:\\
Guardar en el caché las ultimas líneas de la memoria que hayan sido accesadas.\\
Supongamos:
\begin{center}
\begin{verbatim}
         Tc = tiempo de acceso caché
         ! = +- 10 ms
         Tm = Tiempo de acceso a memoria
         ! = 70 ms
         % de éxito = hit rate = hr
         ! = 90 -> 99%
         T acceso:
         Tc <= hr*Tc+(1-hr)*Tm << Tm
\end{verbatim}
\end{center}
El caché contiene una aproximación de las líneas más frecuentemente accesadas. Se requiere un campo adicional que almacene el número de línea que se guarda en una línea del cache.
Costo = Sc * Costo(SRAM) + Sm * Costo (DRAM)\Rigtharrow Costo (DRAM) predomina
\subsubsection{Problema}
Problema: al accesar la memoria hay que “buscar” el número de linea en el caché. Para que sea eficiente hay que buscar en paralelo, lo cual es muy caro
\subsection{Grados de asociatividad}
\begin{itemize}
\item full: una línea de la memoria puede ir en cualquier línea del caché
\item 1: si el caché tiene Lc líneas, la línea $l$ de la memoria solo puede ir en la linea $l$ mod Lc del cache
\item $2n$: se usan $2n$ caches paralelos de 1 grado de asociatividad
\end{itemize}
\subsection{Lectura}
\begin{itemize}
\item Grado 1: Al leer la línea $l$ se examina la etiqueta $n$ de la línea l mod Lc del caché.
\begin{itemize}
\item Si $l = n$ éxito
\item Si $l \neq n$ fracaso. Hay que recuperar la línea de la memoria dinámica y reemplazar la línea $n$ por la $l$
\end{itemize}
\item Grado m$>$1: verificar $l = n$ en todos los cachés
\end{itemize}
\newpage


\subsection{Observaciones}
\begin{itemize}
\item A igual tamaño de caché:
mayor número de grados de asociatividad
\begin{itemize}
\item mejor tasa de aciertos
\item mayor costo
\end{itemize}
\item A igual grado de asociatividad:
mayor tamaño del caché, mejor la tasa de aciertos
\item En la práctica, a igual costo el óptimo en la tasa de aciertos se alcanza en 1, 2, 4 (a 8) grados de asociatividad.
\end{itemize}
\subsection{Políticas para la escritura}
\begin{itemize}
\item La estadística dice que se hacen 3 lecturas por una escritura
\item Write-through: las escrituras se hacen en el caché y en la memoria simultáneamente, pero pueden hacerse sin bloquear a la CPU.
\item Write-back: una escritura solo actualiza el caché. La memoria se actualiza cuando corresponde reemplazar esa línea por otra.
\end{itemize}
\subsection{Ejemplo}
\begin{itemize}
\item Un caché de 1 grado de asociatividad
\item caché de 1024 bytes (1 KB)
\begin{verbatim}
         char buff[128*1024];
         for(;;)
             buff[0] + buff[64*1024]; /* línea mem buff[0] */
                                         = línea mem buff[64*1024]
                                         en el caché */
\end{verbatim}
\item Tasa de aciertos con respectos a los datos = 0\%
\end{itemize}
\newpage


\subsection{Eficiencia}
\begin{itemize}
\item Normalmente, los programas no tienen comportamientos tan desfavorables.
\item La eficiencia del cache dependerá de la localidad en los accesos del programa que se ejecuta.
\item Esta localidad depende de las estructuras de datos que maneja el programa:
\begin{itemize}
\item La pila: el acceso a variables locales tiene un hit rate que supera al 99\%
\item El código presenta muy buena localidad
\item El acceso secuencial de grandes arreglos o matrices (que superan el tamaño del caché) tienen mala localidad.
\end{itemize}
\end{itemize}
\subsection{Problema}
\begin{itemize}
\item Las variables locales y el código del ciclo podrían mapearse en el mismo lugar en el caché
\item La velocidad de ejecución puede caer hasta un 50\%.
\item Este problema no ocurre en un caché de 2 grados de asociatividad.
\item Otra solución es tener un caché separado para:
\begin{itemize}
\item código (instruction caché)
\item datos (data caché).
\end{itemize}
\end{itemize}
\newpage


\subsection{Jerarquía de memoria}
Compromiso:
\begin{itemize}
\item Capacidad
\item Desempeño
\end{itemize}
\begin{center}
\includegraphics[scale=0.55]{f297}
\end{center}

\subsection{Jerarquía de Buses}
Una vez resuelto el problema de la velocidad de la memoria, el bus se transforma en el siguiente cuello de botella:
Se pueden hacer buses económicos en donde es posible conectar CPU, memoria, DMA y todos los dispositivos. Sin embargo el resultado es un bus muy lento por que las señales
tardan demasiado en propagarse. En efecto, para que las señales lleguen a todas las componentes es necesario agragar buffer y transciever que amplifican las señales, pero a su vez introducen un tiempo de retardo adicional. Por otro lado, se pueden hacer buses rápidos, pero que operan a distancias cortas (+- 10
→ 20 cm) y con un número limitado de componentes conectadas al bus.
Solución: Jerarquizar
Las transferencias entre componentes se pueden ordenar:

\section{Pipeling}
\subsection{Partes instrucción}
\begin{itemize}
\item Cada instrucción tiene distintas partes:
\begin{itemize}
\item F  fetch
\item D  decode
\item E  execute
\item W  write-back
\end{itemize}
\item Normalmente, asumimos que cuando se empieza a ejecutar una instruccion, la anterior ya se terminó de ejecutar:
\end{itemize}
\begin{center}
\includegraphics[scale=0.55]{f298}
\end{center}
\newpage

\subsection{Pipeline}
\begin{itemize}
\item Con la solución anterior, se puede ejecutar a lo mas 1 instrucción por cada $N$ ciclos.
\item Usando pipelining, las partes de distintas instrucciones se ejecutan en paralelo:
\begin{center}
\includegraphics[scale=0.55]{f299}
\end{center}
\item En el caso ideal se podría tener 1 instrucción por ciclo
\end{itemize}
\subsection{Hazards}
\begin{itemize}
\item Son problemas que impiden llegar al paralelismo entre instrucciones.
\item Hay 3 tipos:
\begin{itemize}
\item Data Hazards
\item Structural Hazards
\item Control Hazards
\end{itemize}
\end{itemize}
\subsection{Data Hazards}
\begin{itemize}
\item Ocurren cuando distintas instrucciones modifican los mismos datos en distintas etapas del pipeline.
\item Hay 3 subtipos:
\begin{itemize}
\item Lectura después de escritura (RAW)
\item Escritura después de lectura (WAR)
\item Escritura después de escritura (WAW)
\end{itemize}
\end{itemize}
\newpage


\subsubsection{Lectura después de escritura (RAW)}
\begin{itemize}
\item Una instrucción depende del resultado de una instrucción previa.
\item Ej:
\end{itemize}
\begin{center}
\includegraphics[scale=0.55]{f300}
\end{center}
\subsubsection{Escritura después de lectura (WAR)}
\begin{itemize}
\item Un dato es cambiado después de ser utilizado
\item Ej:
\end{itemize}
\begin{center}
\includegraphics[scale=0.55]{f301}
\end{center}
\subsubsection{Escritura después de escritura (WAW)}
\begin{itemize}
\item Un dato es cambiado en dos instrucciones por lo que su valor final depende del orden de ellas
\item Ej:
\end{itemize}
\begin{center}
\includegraphics[scale=0.55]{f302}
\end{center}
\newpage


\subsection{Structural Hazard}
\begin{itemize}
\item Un mismo bus o componente de la CPU es requerida por dos instrucciones simultáneamente
\item Ej:
\end{itemize}
\begin{center}
\includegraphics[scale=0.55]{f303}
\end{center}
\subsection{Control Hazard}
\begin{itemize}
\item Cuando hay un branch, no se sabe cual instrucción se ejecutara a continuación hasta después de evaluar la condición.
\item Ej:
\end{itemize}
\begin{center}
\includegraphics[scale=0.55]{f304}
\end{center}
\subsection{Pipeling}
Simultáneamente:
\begin{itemize}
\item Ejecución de la instrucción actual
\item Carga de la siguiente instrucción
\end{itemize}
Solo se puede hacer un acceso a memoria a la vez. En este caso, load necesita accesar la memoria, por lo que la carga de la siguiente instrucción no puede hacerse en pipelining.
\newpage


\begin{center}
\includegraphics[scale=0.55]{f305}
\end{center}
Los microprocesadores modernos incorporan dos buses (arquitectura de Harvard):
\begin{itemize}
\item uno para datos
\item otro para instrucciones
\end{itemize}
Luego, si se puede cargar la siguiente instrucción en pipelining, siempre y cuando la instrucción este en el caché.
\begin{center}
\includegraphics[scale=0.55]{f306}
\end{center}
La siguiente instrucción se carga, pero no sirve, ya que el salto condicional previo se efectúa.
\begin{center}
\includegraphics[scale=0.6]{f307}
\end{center}
Para evitar que se tenga que botar instrucciones ya cargadas, en algunas arquitecturas se especifica que los saltos son retardados (delayed branch).
La instrucción que sigue un salto se ejecuta aún cuando ocurra el salto.
\newpage


\subsection{Delayed Branches}
\begin{itemize}
\item En los branches, la instrucción siguiente se ejecuta igual
\begin{center}
\includegraphics[scale=0.6]{f308}
\end{center}
\item Es recomendable poner un nop después de un branch
\begin{center}
\includegraphics[scale=0.6]{f309}
\end{center}
\item Eventualmente, se puede poner otra instrucción (riesgoso), pero no otro branch ni set
\end{itemize}
\subsection{Niveles de pipelining}
En la práctica se emplean más de 2 unidades para lograr ejecutar 1 instrucción por ciclo del reloj.
\begin{enumerate}
\item Se usa el valor del registro calculado en la instrucción anterior, que todavía no termina de actualizarse. 
Register Bypassing es una técnica que lleva el resultado de una operación directamente a uno de los operadores de la siguiente instrucción.
\newpage


\begin{center}
\includegraphics[scale=0.6]{f310}
\end{center}
\item El valor de R2 se conocerá el final del ciclo M, se introduce un ciclo de relleno a la espera de R2. (Pipeline Stall, Data Hazard) Register Scoreboarding permite acordarse de los registros con valor desconocido.
\begin{center}
\includegraphics[scale=0.6]{f311}
\end{center}
\newpage


\item Se debe esperar a que la unidad de ejecución se desocupe. (Pipeline Stall, Structural Hazard)
\begin{center}
\includegraphics[scale=0.6]{f312}
\end{center}
\item Se produce un salto por lo que se descarta el trabajo hecho con las 3 siguientes instrucciones.
Los saltos son da\~ninos para la efectividad del pipeline (Branch Hazard)
\begin{center}
\includegraphics[scale=0.6]{f313}
\end{center}
\end{enumerate}
\newpage


\subsection{Branch Prediction}
\begin{itemize}
\item Con altos niveles de pipeline el trabajo que se descarta es inaceptable.
\item Branch prediction es una técnica que intenta de predecir saltos.
\item Por ejemplo los saltos se almacenan en un Branch History Table.
\item Al hacer la decodificación de una instrucción de salto almacenada se sigue cargando instrucciones a partir de la dirección de destino.
\item Actualmente los CPU logran predecir correctamente mas del 95\% de los saltos.
\item Todas estas técnicas permiten acercarse a la barrera de 1 instrucción por tick
\end{itemize}
\section{Arquitecturas superescalares}
\begin{itemize}
\item Son aquellas donde se implementa dentro de la CPU al paralelismo al nivel de instrucciones.
\item Pueden superar la barrera de 1 instrucción por ciclo.
\item Para ello, hay unidades funcionales replicadas dentro del mismo core de CPU.
\end{itemize}
\subsection{Grados de pipelining}
\begin{itemize}
\item Es la cantidad de pipelines en paralelo que se pueden ejecutar.
\item Ejemplo: superescalar de grado 2.
\item Se colocan 2 pipelines en paralelo:
\begin{itemize}
\item Se cargan 2 instrucciones a la vez
\item Se decodifican 2 instrucciones a la vez
\item Se ejecutan 2 instrucciones a la vez
\item Se actualizan 2 registros a la vez
\end{itemize}
\end{itemize}
\newpage


\subsection{Implementación}
Ruta de datos con un pipeline de grado 2:
\begin{center}
\includegraphics[scale=0.6]{f314}
\end{center}
\subsection{Diagrama de pipeline}
Ejemplo de diagrama de pipeline (grado 2):
\begin{center}
\includegraphics[scale=0.6]{f315}
\end{center}
\newpage


\subsection{Dependencia}
En ejecución, puede haber dependencia entre instrucciones que se desean ejecutar simultáneamente
\begin{center}
\begin{verbatim}
              lw $t0, 40($s0)
              add $t1, $t0, %s1
              sub $t0, $s2, $s3
              and $t2, $s4, $t0
              or $t3, $s5, $s6
              sw $s7, 80($t3)
\end{verbatim}
\end{center}
\begin{center}
\includegraphics[scale=0.6]{f317}
\end{center}
\newpage


\subsubsection{Solución}
\begin{itemize}
\item En la etapa de ejecución:
\begin{itemize}
\item Se analizan las dependencias de ambas instrucciones.
\item Si el resultado de la primera instrucción es un operando de la segunda (RAW):
 La ejecución de la segunda se descarta y se repite en el ciclo siguiente
\end{itemize}
\item Los compiladores agregan una pasada que reordena las instrucciones de modo que no existan dependencias de datos.
\end{itemize}
\subsubsection{Evaluación}
\begin{itemize}
\item Ventajas: 1 $<$ IPC $\leq$ numero de pipelines
\item Desventajas:
\begin{itemize}
\item Mucho mas alta complejidad de la CPU.
\item Hay que recompilar para limitar las dependencias.
\end{itemize}
\item Ejemplos:
\begin{itemize}
\item 486: 1 pipeline, 4 etapas (stages)
\item Pentium: 2 pipelines, 5 etapas
\item Pentium Pro, Pentium III: 3 pipelines, 10 etapas
\end{itemize}
\end{itemize}
\subsection{Ejecución fuera de orden}
\begin{itemize}
\item Problema: Compiladores pueden reordenar las instrucciones, pero les falta conocimiento dinámico acerca de cuanto tomara un load, etc.
\item Solución: Ejecución fuera de orden
\item Una dependencia de datos bloquea la instrucción involucrada, pero no las que vienen a continuación si no existen dependencias.
\item Se usa desde el Pentium Pro.
\end{itemize}
\newpage


\begin{center}
\includegraphics[scale=0.6]{f318}
\end{center}
\subsection{Register Renaming}
\begin{itemize}
\item Ataca las dependencias de tipo WAR.
\item Existen varios registros físicos por cada registro de la arquitectura
\end{itemize}
\begin{center}
\includegraphics[scale=0.6]{f319}
\end{center}
\newpage


\subsection{Ejecución especulativa}
\begin{itemize}
\item Se ejecuta el código de un branch antes de saber si se llevará a cabo o no.
\item En caso de no haber branch se restauran los nombres de los registros de modo que los registros modificados vuelven a su valor original.
\end{itemize}
\begin{center}
\includegraphics[scale=0.6]{f320}
\end{center}
\section{Multi-core chips}
\subsection{Symmetric multiprocessing (SMP)}
\begin{center}
\includegraphics[scale=0.6]{f321}
\end{center}
\begin{itemize}
\item Varias CPU en un computador
\item Todas las CPU están conectados a una sola memoria central
\item Cada CPU puede ejecutar un programa y trabajar con datos en cualquier lugar de memoria
\end{itemize}
\newpage


\subsection{Multi Core Chips}
\begin{itemize}
\item Un chip multi-core contiene varios cores
\item Cada core es casi un CPU completo, con incluso sus propios L1 y L2 caché (salvo primeras implementaciones).
\item Un chip N-core puede correr $N$ hilos de ejecucioó en paralelo, siempre que no usen recursos compartidos.
\item Dado que el bus ya es lento para 1 CPU, ¡con varios CPU el problema es peor!
\end{itemize}
\subsubsection{Problema}
\begin{itemize}
\item Todos los cores tienen que ver la misma memoria
\item Varios cores pueden tener la misma línea de memoria en sus propios cachés al mismo tiempo
\item ¿Qué pasa si un core escribe un valor en su caché (L1 o L2)?
\item No es factible dar acceso directo y completo desde el caché de un CPU a otro CPU en sistemas SMP (por velocidad baja del bus).
\end{itemize}
\subsection{Caché Coherence}
\begin{itemize}
\item Mecanismo que permite que todos los cores operen con la misma memoria.
\item La coherencia de cachés se obtiene si se cumplen las siguientes condiciones:
\begin{itemize}
\item Si un procesador escribe en una dirección y luego lee en ella (sin que otro procesador haya escrito en el intertanto) debe leer lo mismo que escribió
\item Si un procesador lee una dirección cuya ultima escritura fue hecha por otro procesador, debe leer el valor escrito por este.
\item Las escrituras deben ser secuenciadas, i.e. leídas en el orden que son escritas.
\end{itemize}
\end{itemize}
\newpage


\subsubsection{Implementación simple}
Write-through cachés:
\begin{itemize}
\item Cada core $C_i$ detecta cuando otro core $C_o$ escribe un valor en su caché.
\item Si esta línea también esta en el caché de $C_i$, es marcada como sucia en $C_i$.
\item Leer una línea sucia requiere cargar el nuevo valor desde la memoria central.
\end{itemize}
\subsubsection{Implementación más sofisticada}
\begin{itemize}
\item Usando bus snooping:
\item Leer una línea sucia significa que la memoria central no esta up to date
\begin{itemize}
\item El core $C_o$ ve que el core $C_i$ lee el valor cambiado usando bus snooping
\item Provee el nuevo valor directamente a $C_i$
\item En ese momento el memory controller también puede escribir este valor en memoria
\end{itemize}
\end{itemize}
\subsection{MESI}
\begin{itemize}
\item Protocolo ampliamente difundido
\item Tiene 4 estados:
\begin{itemize}
\item Modified: El procesador local ha modificado la línea. También implica que es la única copia dentro de los cachés.
\item Exclusive: La línea no ha sido modificada pero se sabe que es la única copia.
\item Shared: La línea no está modificada y podría existir en otros cachés.
\item Invalid: La línea tiene un contenido inválido (estado inicial)
\begin{center}
\includegraphics[scale=0.6]{f322}
\end{center}
\newpage


\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
& M & E & S & I \\
\hline
M & \textcolor{red}{\ding{55}} & \textcolor{red}{\ding{55}}  & \textcolor{red}{\ding{55}} & \textcolor{green}{\ding{51}} \\
\hline
E & \textcolor{red}{\ding{55}}  & \textcolor{red}{\ding{55}}  & \textcolor{red}{\ding{55}}  & \textcolor{green}{\ding{51}} \\
\hline
S & \textcolor{red}{\ding{55}}  & \textcolor{red}{\ding{55}}  & \textcolor{green}{\ding{51}} & \textcolor{green}{\ding{51}}\\
\hline
I & \textcolor{green}{\ding{51}}  & \textcolor{green}{\ding{51}} & \textcolor{green}{\ding{51}} & \textcolor{green}{\ding{51}} \\
\hline
\end{tabular}
\end{center}
\end{itemize}
\end{itemize}
\subsection{Request For Ownership (RFO)}
\begin{itemize}
\item Operación especial
\item Cuando otro core quiere escribir una línea con status (local) Modified, este core manda los datos al otro core y marca la línea (local) como Invalid
\item Cuando este core quiere escribir una línea con status Shared, primero los otros core lo marcan como Invalid
\end{itemize}
\section{Ejercicios}
\subsubsection{Problema 1}
La figura muestra un extracto del contenido de un cache de 8 KB de 2 grados de asociativad y ĺıneas de 16 bytes. El cache se organiza en 2 bancos, cada uno con 256 ĺıneas. Por ejemplo en la ĺınea 3b (en hexadecimal) del banco izquierdo se almacena la linea de memoria
que tiene como etiqueta 53b (es decir, la l ́ınea que va de la dirección  53b0 en hexadecimal a la direcció 53bf).
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
Linea & \multicolumn{2}{|c|}{Banco 1} & \multicolumn{2}{|c|}{Banco 2}\\
\hline
Cache & Etiqueta & Contenido & Etiqueta & Contenido\\
\hline
0a & b0a & & 80a & \\
3b & 53b & & 13b & \\
81 & 481 & & 681 & \\
\hline
\end{tabular}
\caption{Diagrama del Caché}
\label{tab:my_label}
\end{table}

Un programa accede a las siguientes direcciones de memoria: b0a4, 13b0, 10a8, 4810, 6810, 4818, 0b0a, 080a. Indique qué accesos caen en la memoria cache y cuales no. Además muestre un posible estado del cache despu ́es de saquellos accesos.

\textbf{Solución:}

Organizando los accesos a memoria quedaria:
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline
 &  b0a4 & 13b0 & 10a8 & 4810 & 6810 & 4818 & 0b0a & 080a\\
 \hline
 etiqueta cache & b0a & 13b & 10a & 481 & 681 & 481 &0b0 & 080\\
 dir en cache & 0a & 3b & 0a & 81 & 81 & 81 & b0 & 80 \\
 exito? & no & no & no & no & no & si & no & no \\
 \hline
\end{tabular}
\label{tab:my_label}
\end{table}

Para esta secuencia de accesos, una dispoción de memoria cache seria: 

\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline
Linea & \multicolumn{2}{|c|}{Banco 1} & \multicolumn{2}{|c|}{Banco 2}\\
\hline
Cache & Etiqueta & Contenido & Etiqueta & Contenido\\
\hline
0a & b0a & & 80a & \\
3b & 53b & & - & \\
80 & 080 & & - & \\
81 & 481 & & 681 & \\
b0 & 0b0 & & - & \\
\hline
\end{tabular}
\label{tab:my_label}
\end{table}

\subsubsection{Problema 2}
La figura muestra a la izquierda un programa que se ejecuta en un procesador, con una implementación superescalar de grado 2 y 5 etapas en sus 2 pipelines: fetch, decode, analyze, execute y store. Acceder a la memoria toma 2 ciclos del reloj.

\begin{lstlisting}
loop:
    shiftl R1, 2, R2
    load [R6+R2], R3
    add R1, 1, R1
    add R3, R4, R4
    cmp R1, R5
    blt loop
\end{lstlisting}
\begin{table}[h]
\centering
\begin{tabular}{|c| c c c c c|}
\hline
\multirow{2}{*}{Instrucción} & \multicolumn{5}{|c|}{Ciclo} \\
& 1 & 2 & 3 & 4 & 5 \\
 \hline
shiftl R1, 2, R2 & F & D & A & &\\
load [R6+R2], R3 & F & D & A & &\\
add R1, 1, R1 &  & F & D & &\\
add R3, R4, R4  &  & F & D & &\\
\hline
\end{tabular}
\caption{Proceso de Ejecución}
\label{tab:my_label}
\end{table}
Complete el diagrama de ejecución de la derecha, considerando 2 iteraciones del ciclo loop y una arquitectura superescalar con ejecuci ́on fuera de orden.

Luego indique en su diagrama (si es aplicable) en que momentos se usa (a) register bypassing, (b) register scoreboarding, (c) register renaming, (d) predicción de saltos, y (e) ejecución especulativa.


\textbf{Solución: }


El diagrama de ejecución queda como sigue:
%AGREGAR DIAGRAMA
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
\# & \multirow{2}{*}{Instrucción} & \multicolumn{13}{|c|}{Ciclo} \\
\cline{3-15}
& & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 & 13\\
\hline
1 & shiftl R1, 2, R2 & F & D & A & S & & & & & & & & &\\
\hline
2 & add R1, 1, R1 & F & D & A & S & & & & & & & & &\\
\hline
3 & load [R6+R2], R3 &  & F & D & A & E & M & S & & & & & &\\
\hline
4 & add R3, R4, R4 &  & F & D & A &  &  & E & S &  &  &  &  & \\
\hline
5 & cmp R1, R5 & & & F & D & A & E & S & & & & & & \\
\hline
6 & blt loop & & & F & D & A & & & E & & & & &\\
\hline
7 & shiftl R1, 2, R2 & & & & F & F & D & A & E & S & & & & \\
\hline
8 & add R1, 1, R1 & & & & F & F & D & A & & E & S & & &\\
\hline 
9 & load [R6+R2], R3 & & & & & & F & D & A & M & S & & &\\
\hline
10 & add R3, R4, R4 & & & & & & F & D & A & & & E & S & \\
\hline
11 & cmp R1, R5 & & & & & & & F & D & A & E & S & &\\
\hline
12 & blt loop & &  & & & & & F & D & A & & & E &\\
\hline
\end{tabular}
\caption{Proceso de ejecución de las instrucciones}
\label{tab:my_label}
\end{table}


La explicación del diagrama es la siguiente: 
\begin{itemize}
\item De las instrucciones 1 a la 3 se realiza registry bypass para el registro R2.
\item Entre las instrucciones 1 y 2 no es necesario el registry renaming.
\item Entra las instrucciones 3 y 4 se realiza registry scoreboarding del registro R3.
\item en la instrucción 6 a la 7 y 8 se realiza una branch prediction. Las primeras fases fetch de las instrucciones 6 y 7 se eliminan por la prediccíon del branch.
\item Entre las instrucciones 9 y 10 se realiza un registry scoreboarding del registro R3.
\item Observemos que las instrucciones de branch se ejecutan DESPUES de la fase de store de la instrucción de comparación, ya que es en esa fase en el cual se escribe el registro de flags, necesarios para determinar el branch.
\end{itemize}
\subsubsection{Problema 3}
La siguiente es una secuencia de direcciones de memoria (en hexadecimal) leídas por un procesador con una memoria cache de 4 KB (2^{12} bytes) de un grado de asociatividad:


5F30  6D18  5F30  7F30  6D10  7F30  5F30  6D10

El cache posee líneas de 16 bytes. Suponga que el cache está  inicialmente vacío.
\begin{enumerate}
\item ¿Cuál es la porción de la dirección que se usa como etiqueta?
\item ¿Cuál la porción de la dirección que se usa para indexar el cache?
\item ¿Qué accesos a la memoria son aciertos en el cache y cuáles son desaciertos?
\end{enumerate}
\subsubsection{Problema 4}
La tabla muestra ciclo por ciclo la ejecución de varias instrucciones en un procesador.
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
\# & \multirow{2}{*}{Instrucción} & \multicolumn{15}{|c|}{Ciclo} \\
\cline{3-17}
& & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 & 13 & 14 & 15 \\
\hline
a & ADD R1, 4, R1 & F & D & A & E & S & & & & & & & & & &\\
\hline
b & LDW [R2+8], R3 & F & D & A & E & M & M & M & M & M & S & & & & & \\
\hline
c & OR R1, 255, R4 &  & F & D & A & E & S &  & & & & & & & &\\
\hline
d & ADD R4, 1, R2 &  & F & D & A &  & E & S &  &  &  & & &  & &\\
\hline
e & CMP R3, 0 & & & F & D & A &  &  & & & & E & & & & \\
\hline
f & BNE a.- & & & F & D & A & & &  & & & & E & & & \\
\hline
a' & ADD R1, 4, R1 & & & & F & D & A & & & & & & & E & S  &\\
\hline
b' & LDW [R2+8], R3 & & & & F & D & A &  & &  &  & & & E & M & S \\
\hline 
\end{tabular}
\caption{Proceso de ejecución de las instrucciones}
\label{tab:my_label}
\end{table}
\begin{enumerate}
\item Indique en qué momentos se recurre (si es que se recurre) a las siguientes técnicas: register bypassing, register scoreboarding, ejecucíon superescalar y renombre de registros.
\item Modifique la tabla de m ́as arriba considerando que la predicción del salto f.- fue errónea. Invente sus propias instrucciones g.- y h.-. Indique solo las filas y columnas que hay que modificar en la tabla.
\item Rehaga la tabla de más arriba considerando un procesador de similares caracter ́ısticas pero con ejecución  fuera de orden y ejecución especulativa. Explique además en qué momentos se recurre a renombre de registros y ejecución especulativa. No olvide agregar la fase de retiro en el pipeline.
\end{enumerate}
\section{Anexos}
Los videos de las clases son los siguientes:
\begin{itemize}
\item \href{https://www.youtube.com/watch?v=iIdHXqsh67Q&list=UUYoDv0XY522Qmnae2Up1jJQ}{Clase 22: Memoria caché}
\item \href{https://www.youtube.com/watch?v=mR5mYlmkaAA&index=4&list=UUYoDv0XY522Qmnae2Up1jJQ}{Clase 23: Pipelining}
\end{itemize}





